//have the camera move by deltaX, deltaY
void moveCamera(int8_t deltaX, int8_t deltaY) {
  g_xCam -= deltaX;
  g_yCam -= deltaY;
}
