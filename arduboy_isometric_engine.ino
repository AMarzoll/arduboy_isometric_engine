#define DEBUGGERINO
//#define SCREENDUMP

#include "Globals.h"
#include "SpriteData.h"
#include "TileData.h"
#include "WorldData.h"
#include "MoveableObject.h"
#include "Player.h"
#include "NPC.h"
#include "moveCamera.h"
#include "updateWorld.h"
#include "drawWorld.h"
#include "playerInput.h"



////////////////////////////////////////////////////////////////////
/*                    ENTRY & PROGRAM LOOP                        */
////////////////////////////////////////////////////////////////////


void setup() {
  adb.begin();
  adb.initRandomSeed();
  adb.clear();
  adb.setFrameRate(30);
  randomSeed(analogRead(0));
  #ifdef SCREENDUMP
  Serial.begin(9600);
  #endif

  //initialize some player-related variables
  player.x_ = 56;
  player.y_ = 56;
  player.z_ = player.getZCoords();
  player.centerCam();
  //npc1.setTile(5,3);
  //npc2.setTile(8,7);
}

void loop() {
  #ifdef DEBUGGERINO
  if (!(adb.nextFrameDEV())) return;
  #endif
  #ifndef DEBUGGERINO
  if (!(adb.nextFrame())) return;
  #endif
  adb.clear();
  adb.pollButtons();

  switch (g_gameStatus) {
    case GameStatus::Title:
      break;
    case GameStatus::InGame:
      updateWorld();
      drawWorld();
      playerInput();
      break;
    case GameStatus::GameOver:
      break;
  }
  #ifdef SCREENDUMP
  Serial.write(adb.getBuffer(), 128 * 64 / 8);
  #endif
  adb.display();
}
