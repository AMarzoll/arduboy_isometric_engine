////////////////////////////////////////////////////////////////////
/*                       NPC CLASS                                */
////////////////////////////////////////////////////////////////////

class NPC: public MoveableObject {
  public:
    enum NPCSprite {
      NEspr,
      SEspr,
      SWspr,
      NWspr,
    };
    NPCSprite npcSprite = NPCSprite::NEspr;

    NPC() {
      sprites_[0] = sprite_NE;
      sprites_[1] = sprite_SE;
      sprites_[2] = sprite_SW;
      sprites_[3] = sprite_NW;

      spriteMasks_[0] = sprite_NE_mask;
      spriteMasks_[1] = sprite_SE_mask;
      spriteMasks_[2] = sprite_SW_mask;
      spriteMasks_[3] = sprite_NW_mask;

    }
};
//NPC npc1, npc2;
