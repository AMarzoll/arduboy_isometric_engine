////////////////////////////////////////////////////////////////////
/*                     PLAYER INPUT                               */
////////////////////////////////////////////////////////////////////

//#define DEBUGGERINO

void playerInput() {
  if (adb.pressed(LEFT_BUTTON) && adb.notPressed(RIGHT_BUTTON) && adb.notPressed(UP_BUTTON) && adb.notPressed(DOWN_BUTTON)) {
    if (player.attemptMove(-2, -1)) {
      moveCamera(-2, -1);
    }
    player.playerSprite = player.PlayerSprite::NWspr;
    player.swapAnimationFrame();
  }
  if (adb.pressed(RIGHT_BUTTON) && adb.notPressed(LEFT_BUTTON) && adb.notPressed(UP_BUTTON) && adb.notPressed(DOWN_BUTTON)) {
    if (player.attemptMove(+2, +1)) {
      moveCamera(+2, +1);
    }
    player.playerSprite = player.PlayerSprite::SEspr;
    player.swapAnimationFrame();
  }
  if (adb.pressed(UP_BUTTON) && adb.notPressed(RIGHT_BUTTON) && adb.notPressed(LEFT_BUTTON) && adb.notPressed(DOWN_BUTTON)) {
    if (player.attemptMove(+2, -1)) {
      moveCamera(+2, -1);
    }
    player.playerSprite = player.PlayerSprite::NEspr;
    player.swapAnimationFrame();
  }
  if (adb.pressed(DOWN_BUTTON) && adb.notPressed(RIGHT_BUTTON) && adb.notPressed(UP_BUTTON) && adb.notPressed(LEFT_BUTTON)) {
    if (player.attemptMove(-2, +1)) {
      moveCamera(-2, +1);
    }
    player.playerSprite = player.PlayerSprite::SWspr;
    player.swapAnimationFrame();
  }
#ifdef DEBUGGERINO
  if (adb.pressed(A_BUTTON)) {
    adb.fillRect(0, 0, 30, 39, BLACK);
    adb.setCursor(0, 0);
    adb.print(player.x_);
    adb.setCursor(0, 7);
    adb.print(player.y_);
    adb.setCursor(0, 15);
    adb.print(static_cast<int16_t>(player.getXTileCoords()));
    adb.setCursor(0, 23);
    adb.print(static_cast<int16_t>(player.getYTileCoords()));
    adb.setCursor(0, 31);
    adb.print(static_cast<int16_t>(player.z_));
  }
#endif
#ifdef DEBUGGERINO

static int16_t cpuLoad = 0;

  if (adb.pressed(B_BUTTON)) {
    adb.fillRect(WIDTH - 20, 0, 20, 8, BLACK);
    adb.setCursor(WIDTH - 20, 0);
    if (adb.everyXFrames(30))
      cpuLoad = adb.cpuLoad();
    adb.print(cpuLoad);
    //adb.setRGBled(8, 8, 0);
    //adb.invert(true);
  }
#endif
  if (adb.justPressed(B_BUTTON)) {
    player.isJump_ = true;
  }
}
