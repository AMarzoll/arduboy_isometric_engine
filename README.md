# Arduboy Isometric Engine

A simple engine for rendering an isometric landscape on the Arduboy game system (based on the Arduino Leonardo microcontroller, see https://www.arduboy.com/). Supports different levels of elevation and moveable objects such as player characters or enemies. This is work in progress and could be used for any kind of game genre. At this moment, most stuff happens in the `MoveableObject` class and in `drawWorld()`.

![Arduboy Isometric Engine](/arduboy_isometric_screenshot.png "sample screenshot")