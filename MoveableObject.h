#ifndef MOVEABLEOBJECT_H
#define MOVEABLEOBJECT_H

////////////////////////////////////////////////////////////////////
/*             MOVABLE OBJECT CLASS DECLARATION                   */
////////////////////////////////////////////////////////////////////

class MoveableObject;

class MoveableObjectList {
  public:
    MoveableObject*  list[0];
  private:
    uint8_t numel = 0;
};


class MoveableObject {
  public:
	//data members
    int16_t   x_;
    int16_t   y_;
    uint8_t   z_;
    uint8_t   heightAtOrigin_ = 0;
    const uint8_t*  sprites_[4];
    const uint8_t*  spriteMasks_[4];
    uint8_t   animationFrame_ = 0;
    bool      isJump_ = false;
    uint8_t   jumpFrame_ = 0;

	//function members
    inline int8_t    getXTileCoords(); //convert pixelwise (x,y)-coordinates into isometric tile coordinates (automatic round-off)
    inline int8_t    getYTileCoords();
    inline int8_t    getZCoords();  //get current height of tile the MOB is standing on    
    int8_t    getHeightDiff(int16_t targetX, int16_t targetY); //calculate height difference between current MOB position and some (targetX,targetY)
    bool      attemptMove(int8_t deltaX, int8_t deltaY); //attempt to move one step (+deltaX, +deltaY) ahead, return true if attempt succeeds, otherwise return false
    void      swapAnimationFrame();
    void      draw(const int16_t spriteID); //draw moveable object
    void      centerCam();
    void      setTile(uint8_t xTile, uint8_t yTile);
};

////////////////////////////////////////////////////////////////////
/*           MOVABLE OBJECT CLASS IMPLEMENTATION                  */
////////////////////////////////////////////////////////////////////


    int8_t
    MoveableObject::getXTileCoords() {
      return (x_ + 2 * y_) / 32;
    }


    
    int8_t
    MoveableObject::getYTileCoords() {
      return (-x_ + 2 * y_) / 32;
    }



    int8_t
    MoveableObject::getZCoords() {
      return g_zStep * pgm_read_byte(&heightmap[getYTileCoords()][getXTileCoords()]);
    }



    int8_t
    MoveableObject::getHeightDiff(int16_t targetX, int16_t targetY) {
      return g_zStep * (pgm_read_byte(&heightmap[(2 * y_ - x_) / 32][(x_ + 2 * y_) / 32]) - pgm_read_byte(&heightmap[(2 * targetY - targetX) / 32][(targetX + 2 * targetY) / 32]));
    }



    bool
    MoveableObject::attemptMove(int8_t deltaX, int8_t deltaY) {
      //return if MOB would move outside the playing field
      if (((x_ + deltaX) + 2 * (y_ + deltaY) < 0) ||
          ((x_ + deltaX) + 2 * (y_ + deltaY) >= g_worldWidth * g_tileWidth) ||
          (2 * (y_ + deltaY) - (x_ + deltaX) < 0) ||
          (2 * (y_ + deltaY) - (x_ + deltaX) >= g_worldHeight * g_tileWidth)) {
        return false;
      }
      //calculate z_-level difference between current position and next
      int8_t heightDiff = getHeightDiff(x_ + deltaX, y_ + deltaY);
      int8_t signX = (deltaX > 0) ? 1 : -1;
      int8_t signY = (deltaY < 0) ? 1 : -1;
      constexpr int8_t paddingFactor = 3;
      int8_t heightDiffPadded = getHeightDiff(x_ + (paddingFactor * deltaX),
                                              y_ + (paddingFactor * deltaY));

      //if there is a ledge and the MOB is not jumping, initiate fall sequence by starting the jump sequence towards the end
      if (heightDiff > 0 && !isJump_) {
        isJump_ = true;
        heightAtOrigin_ = getZCoords();
        jumpFrame_ = g_jumpTableZeroIndex; 
      }

      //move into the next tile if either the height difference is at most one g_zStep or is at most zero when jumping
      if ((!isJump_ && heightDiffPadded >= -g_zStep) || (isJump_ && (getZCoords() - heightDiff <= z_))) {
        x_ += deltaX;
        y_ += deltaY;
        return true;
      }
      return false;
    }



    void
    MoveableObject::swapAnimationFrame() {
      if (adb.everyXFrames(3)) {
        if (animationFrame_ == 0) {
          animationFrame_ = 1;
        }
        else animationFrame_ = 0;
      }
    }



    void
    MoveableObject::draw(const int16_t spriteID) {
      int8_t zOffset = 0;

      //handle jump
      if (isJump_) {
        //if this is the first frame of the jump, memorize initial height
        if (jumpFrame_ == 0) {
          heightAtOrigin_ = getZCoords();
        }

        //calculate jump height to be drawn
        zOffset = pgm_read_byte(&g_jumpTable[jumpFrame_]) - (getZCoords() - heightAtOrigin_);
        jumpFrame_++;

        

        //check if MOB has landed, which ends the jump
        if (zOffset <= 0) {
          jumpFrame_ = 0;
          isJump_ = false;
          zOffset = 0;
        }
      }

      
      
      //calculate z_-coordinate, taking current jump-height and world height into account
      z_ = pgm_read_byte(&heightmap[getYTileCoords()][getXTileCoords()]) * g_zStep + zOffset;

      //draw shadow
      //adb.fillCircle(WIDTH/2-1, HEIGHT/2-getZCoords()+5, 2);
      uint8_t shadowSize = (z_ - getZCoords() > 8) ? 3 : 4;
      adb.fillRoundRect(WIDTH/2-3, HEIGHT/2-getZCoords()+4, shadowSize+1, shadowSize, 1);

      //actually draw the mob
      Sprites::drawExternalMask(x_ + g_xCam + WIDTH / 2 - g_playerWidth / 2,
                                y_ + g_yCam - g_playerHeight - z_,
                                sprites_[spriteID],
                                spriteMasks_[spriteID],
                                animationFrame_,
                                animationFrame_);
    }


    
    void
    MoveableObject::centerCam() {
      g_xCam = 0 - x_;
      g_yCam = 40 - y_;
    }


    
    void
    MoveableObject::setTile(uint8_t xTile, uint8_t yTile) {
       x_ = g_tileWidth/2 * (xTile - yTile + 1);
       y_ = g_tileHeight/2 * (xTile + yTile + 0.5);
       z_ = getZCoords();
    }

#endif
