////////////////////////////////////////////////////////////////////
/*                  DRAW WORLD AND ITS CONTENTS                   */
////////////////////////////////////////////////////////////////////

void drawWorld() {

  static uint8_t frame = 0;
  static uint8_t wtrFrame = 1;
  static bool wtrRising = true;

  //water animation
  if (adb.everyXFrames(30)) {
    if (wtrRising) {
      wtrFrame++;
      if (wtrFrame == 2) {
        wtrRising = false;
      }
    } else {
      wtrFrame--;
      if (wtrFrame == 0) {
        wtrRising = true;
      }
    }
  }

  //center tile of drawing
  int8_t ctrXTile = player.getXTileCoords();
  int8_t ctrYTile = player.getYTileCoords();

  //initialize deltaZ
  static int8_t deltaZleft = 0;
  static int8_t deltaZright = 0;
  static int8_t deltaZbottom = 0;

  constexpr uint8_t drawRadius = 5;

  //consider only tiles ±drawRadius in every direction from center for drawing
  for (int8_t yTile = max(ctrYTile - drawRadius, 0); yTile < min(ctrYTile + drawRadius, g_worldHeight); yTile++) {
    for (int8_t xTile = max(ctrXTile - drawRadius, 0); xTile < min(ctrXTile + drawRadius, g_worldWidth); xTile++) {
      
      //if tile is outside drawing range, continue to next tile
      if((abs(ctrXTile - xTile) + abs(ctrYTile - yTile)) > (drawRadius + 1)) {
        continue;
      }

      //calculate upper left x and y coordinates on the screen for the current tile's drawing process
      int8_t xDraw = ((xTile - yTile + 1) * g_tileWidth / 2) + (HEIGHT / 2) + g_xCam;
      int8_t yDraw = ((xTile + yTile) * g_tileHeight / 2 + g_yCam) - pgm_read_byte(&heightmap[yTile][xTile]) * g_zStep;

      //if tile is a water tile, get its animation frame
      if((pgm_read_byte(&world[yTile][xTile]) == T::W)) {
        frame = wtrFrame;
      } else {
        frame = 0;
      }

      //draw map tile
      Sprites::drawExternalMask(xDraw,
                                yDraw,
                                tiles[pgm_read_byte(&world[yTile][xTile])],
                                tile_mask,
                                frame,
                                0);
                            

      //draw vertical lines in front of tiles with nonzero height where appropriate (taking neighboring tiles int16_to acct)
      uint8_t curTileHeight = pgm_read_byte(&heightmap[yTile][xTile]);
      uint8_t southwestTileHeight = (g_worldHeight > yTile+1) ? pgm_read_byte(&heightmap[yTile+1][xTile]) : 0;
      uint8_t southeastTileHeight = (g_worldWidth  > xTile+1) ? pgm_read_byte(&heightmap[yTile][xTile+1]) : 0;
      uint8_t southTileHeight = (g_worldWidth > xTile+1)&&(g_worldHeight > yTile+1) ? pgm_read_byte(&heightmap[yTile+1][xTile+1]) : 0;
      
      if((curTileHeight > 0) && (southwestTileHeight < curTileHeight) || (southeastTileHeight < curTileHeight)) {

        //mask out faces, so tiles drawn earlier don't "shine through"
        
        Sprites::drawErase(xDraw,
                           yDraw + g_tileHeight / 2,
                           face_mask,
                           0);
                           
        /*Sprites::drawExternalMask(xDraw,
                                  yDraw + g_tileHeight / 2,
                                  face,
                                  face_mask,
                                  0,
                                  0);*/
        
        //calculate length of vertical lines according to lower left([+1][+0]), lower right([+0][+1]) and lower central ([+1][+1]) neighbors
        //max() ensures that lines only get drawn downward
        //ensure we only use the heightmap-value for the tiles on the bottom-left/bottom-right instead of the difference to the neighbors
        if ((yTile < (g_worldHeight - 1)) && (xTile < (g_worldWidth - 1))) {
          deltaZleft = max((curTileHeight - southwestTileHeight), 0);
          deltaZright = max((curTileHeight - southeastTileHeight), 0);
          deltaZbottom = max((curTileHeight - southTileHeight), 0);
          //handle bottom left and bottom right sides separately
        } else {
          deltaZleft = curTileHeight;
          deltaZright = curTileHeight;
          deltaZbottom = curTileHeight;
        }
        //draw the vertical lines at the appropriate length

        
        /*adb.drawFastVLine(xDraw,
                          yDraw + (g_tileHeight / 2),
                          g_zStep * deltaZleft);

        adb.drawFastVLine(xDraw + g_tileWidth - 1,
                          yDraw + (g_tileHeight / 2),
                          g_zStep * deltaZright);

        adb.drawFastVLine(xDraw + (g_tileWidth / 2) - 1,
                          yDraw + g_tileHeight,
                          g_zStep * deltaZbottom);

        adb.drawFastVLine(xDraw + (g_tileWidth / 2),
                          yDraw + g_tileHeight,
                          g_zStep * deltaZbottom);*/

        for(int steps = 0; steps < g_zStep*deltaZleft; steps+=2) {
          adb.drawPixel(xDraw, yDraw + (g_tileHeight/2) + steps);
        }

        for(int steps = 0; steps < g_zStep*deltaZright; steps+=2) {
          adb.drawPixel(xDraw + g_tileWidth - 1, yDraw + (g_tileHeight/2) + steps);
        }
        for(int steps = 0; steps < g_zStep*deltaZbottom; steps+=2) {
          adb.drawPixel(xDraw + (g_tileWidth / 2) - 1, yDraw + g_tileHeight + steps);
          adb.drawPixel(xDraw + (g_tileWidth / 2), yDraw + g_tileHeight + steps);
        }

        
      }
      

      //if player is on this tile, draw player
      //in a future version, the program may cycle through a list of all MOBs to draw on the playing field here
      if (xTile == player.getXTileCoords() && yTile == player.getYTileCoords()) {
        player.draw(player.playerSprite);
      }
      /*if (xTile == npc1.getXTileCoords() && yTile == npc1.getYTileCoords()) {
        npc1.draw(npc1.npcSprite);
      }
      if (xTile == npc2.getXTileCoords() && yTile == npc2.getYTileCoords()) {
        npc2.draw(npc2.npcSprite);
      }*/
    }
  }
  //draw diagonal lines at the very left and right bottom of the world map
  if(ctrYTile > g_worldHeight - drawRadius) {
    adb.drawLine(((0 - g_worldHeight + 2) * g_tileWidth / 2) + (HEIGHT / 2) + g_xCam,
                 ((0 + g_worldHeight) * g_tileHeight / 2 + g_yCam),
                 ((g_worldWidth - g_worldHeight + 2) * g_tileWidth / 2) + (HEIGHT / 2) + g_xCam - 1 ,
                 (g_worldWidth + g_worldHeight) * g_tileHeight / 2 + g_yCam - 1,
                 WHITE);
  }
    
  if(ctrXTile > g_worldWidth - drawRadius) {
    adb.drawLine(((g_worldWidth + 2) * g_tileWidth / 2) + (HEIGHT / 2) + g_xCam - 1,
                 ((g_worldWidth) * g_tileHeight / 2 + g_yCam),
                 ((g_worldWidth - g_worldHeight + 2) * g_tileWidth / 2) + (HEIGHT / 2) + g_xCam,
                 (g_worldWidth + g_worldHeight) * g_tileHeight / 2 + g_yCam - 1,
                 WHITE);
  }
}
