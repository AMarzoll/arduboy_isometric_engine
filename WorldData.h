#ifndef WORLDDATA_H
#define WORLDDATA_H

////////////////////////////////////////////////////////////////////
/*                        WORLD DATA                              */
////////////////////////////////////////////////////////////////////


enum T {
  B, //blank
  G, //grass
  W, //water
};

const uint8_t world[g_worldHeight][g_worldWidth] PROGMEM = {
  {T::G, T::G, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::G, T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::G, T::B  },
  {T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::W, T::W, T::W, T::W, T::B, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::B, T::W, T::W, T::W, T::B, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::G, T::B, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B  },
  {T::G, T::B, T::W, T::B, T::G, T::B, T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::G  },
  {T::G, T::B, T::B, T::W, T::B, T::G, T::B, T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::G  },
  {T::B, T::B, T::W, T::W, T::B, T::G, T::B, T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::G  },
  {T::G, T::B, T::B, T::W, T::B, T::G, T::G, T::B, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B  },
  {T::B, T::B, T::B, T::W, T::W, T::B, T::G, T::B, T::B, T::B, T::B, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B  },
  {T::B, T::B, T::W, T::W, T::W, T::W, T::B, T::W, T::W, T::B, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::B, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::W, T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::B, T::B, T::B, T::B  },
  {T::W, T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
  {T::W, T::W, T::W, T::B, T::B, T::B, T::B, T::B, T::B, T::B, T::G, T::B, T::G, T::B, T::B, T::B, T::G, T::B, T::G, T::B  },
};

//values in the heightmap should lie between 0 and g_zLevelsMax-1
const uint8_t heightmap[g_worldHeight][g_worldWidth] PROGMEM = {
  {4, 4, 3, 2, 2, 1, 0, 0, 0, 0, 1, 1, 2, 2, 1, 1, 2, 2, 3, 1 },
  {4, 1, 3, 3, 1, 1, 0, 0, 0, 0, 0, 2, 3, 2, 1, 1, 3, 4, 4, 2 },
  {2, 3, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1 },
  {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 2 },
  {2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 1, 2, 2, 3 },
  {4, 2, 0, 0, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 2, 1, 1, 2, 2 },
  {4, 2, 0, 1, 4, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 2, 4 },
  {4, 1, 1, 0, 3, 4, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 2, 2, 4 },
  {3, 1, 0, 0, 3, 4, 2, 2, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 2, 4 },
  {4, 2, 1, 0, 2, 4, 4, 3, 2, 1, 1, 1, 0, 0, 0, 2, 1, 1, 2, 3 },
  {3, 2, 1, 0, 0, 2, 4, 2, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 3 },
  {3, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 2, 1, 1, 2 },
  {3, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 },
  {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 4, 1, 4, 1, 1, 1, 1, 2, 3, 3 },
  {0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 2, 3, 3, 3, 4 },
  {0, 2, 0, 1, 1, 1, 1, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 3, 0, 0, 1, 2, 1, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 4, 0, 1, 1, 1, 1, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 1, 0, 2, 1, 2, 1, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 2, 0, 1, 2, 3, 2, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 4, 2, 3, 3, 4, 3, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 0, 0, 1, 2, 3, 2, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 0, 0, 1, 1, 2, 1, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
  {0, 0, 0, 1, 1, 1, 1, 2, 3, 2, 4, 1, 4, 1, 2, 2, 4, 3, 4, 3 },
};

#endif
