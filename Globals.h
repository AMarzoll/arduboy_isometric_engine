#ifndef GLOBALS_H
#define GLOBALS_H

#include <Arduboy2.h>

Arduboy2 adb;
constexpr uint8_t g_tileWidth = 32;
constexpr uint8_t g_tileHeight = 16;
constexpr uint8_t g_worldWidth = 20;
constexpr uint8_t g_worldHeight = 24;
constexpr uint8_t g_playerWidth = 16;
constexpr uint8_t g_playerHeight = 16;
constexpr uint8_t g_zStep = 3;
constexpr uint8_t g_zLevelsMax = 5;
constexpr uint8_t g_drawPadding = 7;
constexpr uint8_t g_jumpTableZeroIndex = 19;

int16_t g_xCam;
int16_t g_yCam;

//lookup table for jumps
//const int8_t g_jumpTable[] PROGMEM = {1, 2, 3, 3, 4, 4, 5, 5, 6, 6, 6, 5, 5, 4, 4, 3, 3, 2, 1, 0, -2, -4, -6, -9, -12, -15, -64};
const int8_t g_jumpTable[] PROGMEM = {1, 2, 4, 6, 8, 10, 11, 12, 12, 12, 11, 11, 10, 8, 6, 4, 3, 2, 1, 0, -2, -4, -6, -9, -12, -15, -64};


enum GameStatus {
  Title,
  InGame,
  GameOver,
};
GameStatus g_gameStatus = InGame;

#endif
